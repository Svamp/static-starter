# Spinon - static starter

## Getting started 

Clone this repository

Install dependencies 

``` sh
$ npm install
```

*Note: this repository includes a [yarn.lock](https://yarnpkg.com/lang/en/) file*

You're ready to start !

### Development 

To start webpack development server use : 

```sh
$ npm run dev
```

*Note: server runs on port 7000 by default*

The server listens to every change in .html/.php, .scss and .js files and reloads your browser on save.

To build use : 

```sh
$ npm run build
```