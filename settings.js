module.exports = {
    entry: ['./src/js/app.js', './src/sass/app.scss', './src/jade/index.pug'],
    browsers: ['last 2 versions', 'ie >= 9'],
    port: 7000,
    proxy: "http://domain.local"
}
